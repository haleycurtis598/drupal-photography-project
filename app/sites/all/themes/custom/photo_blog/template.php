<?php

function photo_blog_preprocess_page(&$variables){
  // print_r($variables);
  $page = $variables["page"];
  $body_class = "full-width";
  $column_class = "";

  if(!empty($page["left_column"])){
    $column_class = "one-fourth";
    $body_class = "half";
  }

  if(!empty($page["right_column"])){
    $column_class = "one-fourth";
    $body_class = "half";
  }

  $variables["photo_blog"]["body_class"] = $body_class;
  $variables["photo_blog"]["column_class"] = $column_class;

}
